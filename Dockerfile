FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} fizzbuzz.jar
ENTRYPOINT ["java","-jar","/fizzbuzz.jar"]