package com.mpo.fizzbuzz.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {FizzBuzzService.class})
public class FizzBuzzServiceTest {
	
	private final static int FIZZ_REPLACED_CLASSIC_FIZZ_BUZZ = 3;
	private final static int BUZZ_REPLACED_CLASSIC_FIZZ_BUZZ = 5;
	private final static int LIMIT_VALUE_CLASSIC_FIZZ_BUZZ = 100;
	private final static String FIZZ_REPLACING_CLASSIC_FIZZ_BUZZ = "fizz";
	private final static String BUZZ_REPLACING_CLASSIC_FIZZ_BUZZ = "buzz";
	
	@Autowired
    private FizzBuzzService fizzBuzzService;

	@Test
	public void classicFizzBuzzTest() throws FileNotFoundException {
		List<String> expectedFizzBuzzList = readListFromFile("./src/test/resources/results/ClassicFizzBuzz.txt");
		
		List<String> actualFizzBuzzList = fizzBuzzService.generateFizzBuzz(FIZZ_REPLACED_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACED_CLASSIC_FIZZ_BUZZ, LIMIT_VALUE_CLASSIC_FIZZ_BUZZ, FIZZ_REPLACING_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACING_CLASSIC_FIZZ_BUZZ);
		
		assertEquals(expectedFizzBuzzList, actualFizzBuzzList);
	}

	@Test
	public void leBonCoinFizzBuzzTest() throws FileNotFoundException {
		List<String> expectedFizzBuzzList = readListFromFile("./src/test/resources/results/LeBonCoinFizzBuzz.txt");
		
		List<String> actualFizzBuzzList = fizzBuzzService.generateFizzBuzz(4, 7, 1000, "LeBon", "Coin");
		
		assertEquals(expectedFizzBuzzList, actualFizzBuzzList);
	}
	
	@ParameterizedTest
	@ValueSource(ints = {FizzBuzzService.MIN_FIZZ_BUZZ_LIMIT, LIMIT_VALUE_CLASSIC_FIZZ_BUZZ, FizzBuzzService.MAX_FIZZ_BUZZ_LIMIT})
	public void limitTest(int limit) {
		List<String> actualFizzBuzzList = fizzBuzzService.generateFizzBuzz(FIZZ_REPLACED_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACED_CLASSIC_FIZZ_BUZZ, limit, FIZZ_REPLACING_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACING_CLASSIC_FIZZ_BUZZ);
		
		assertEquals(limit, actualFizzBuzzList.size());
	}
	
	@ParameterizedTest
	@ValueSource(ints = {FizzBuzzService.MIN_FIZZ_BUZZ_LIMIT -1, FizzBuzzService.MAX_FIZZ_BUZZ_LIMIT + 1})
	public void outOfRangeLimitTest(int limit) {
		assertThrows(IllegalArgumentException.class, () -> fizzBuzzService.generateFizzBuzz(FIZZ_REPLACED_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACED_CLASSIC_FIZZ_BUZZ, limit, FIZZ_REPLACING_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACING_CLASSIC_FIZZ_BUZZ));
	}
	
	@ParameterizedTest
	@ValueSource(ints = {0, FizzBuzzService.MIN_FIZZ_BUZZ_REPLACED -1, FizzBuzzService.MAX_FIZZ_BUZZ_REPLACED + 1})
	public void outOfRangeFizzReplacedTest(int fizzReplaced) {
		assertThrows(IllegalArgumentException.class, () -> fizzBuzzService.generateFizzBuzz(fizzReplaced, BUZZ_REPLACED_CLASSIC_FIZZ_BUZZ, LIMIT_VALUE_CLASSIC_FIZZ_BUZZ, FIZZ_REPLACING_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACING_CLASSIC_FIZZ_BUZZ));
	}
	
	@ParameterizedTest
	@ValueSource(ints = {0, FizzBuzzService.MIN_FIZZ_BUZZ_REPLACED -1, FizzBuzzService.MAX_FIZZ_BUZZ_REPLACED + 1})
	public void outOfRangeBuzzReplacedTest(int buzzReplaced) {
		assertThrows(IllegalArgumentException.class, () -> fizzBuzzService.generateFizzBuzz(FIZZ_REPLACED_CLASSIC_FIZZ_BUZZ, buzzReplaced, LIMIT_VALUE_CLASSIC_FIZZ_BUZZ, FIZZ_REPLACING_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACING_CLASSIC_FIZZ_BUZZ));
	}
	
	@Test
	public void nullFizzStringTest() {
		assertThrows(IllegalArgumentException.class, () -> fizzBuzzService.generateFizzBuzz(FIZZ_REPLACED_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACED_CLASSIC_FIZZ_BUZZ, LIMIT_VALUE_CLASSIC_FIZZ_BUZZ, null, BUZZ_REPLACING_CLASSIC_FIZZ_BUZZ));
	}
	
	@Test
	public void nullBuzzStringTest() {
		assertThrows(IllegalArgumentException.class, () -> fizzBuzzService.generateFizzBuzz(FIZZ_REPLACED_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACED_CLASSIC_FIZZ_BUZZ, LIMIT_VALUE_CLASSIC_FIZZ_BUZZ, FIZZ_REPLACING_CLASSIC_FIZZ_BUZZ, null));
	}
	
	@Test
	public void emptyFizzStringTest() {
		assertThrows(IllegalArgumentException.class, () -> fizzBuzzService.generateFizzBuzz(FIZZ_REPLACED_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACED_CLASSIC_FIZZ_BUZZ, LIMIT_VALUE_CLASSIC_FIZZ_BUZZ, "", BUZZ_REPLACING_CLASSIC_FIZZ_BUZZ));
	}
	
	@Test
	public void emptyBuzzStringTest() {
		assertThrows(IllegalArgumentException.class, () -> fizzBuzzService.generateFizzBuzz(FIZZ_REPLACED_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACED_CLASSIC_FIZZ_BUZZ, LIMIT_VALUE_CLASSIC_FIZZ_BUZZ, FIZZ_REPLACING_CLASSIC_FIZZ_BUZZ, ""));
	}
	
	private List<String> readListFromFile(String filePath) throws FileNotFoundException {
		ArrayList<String> list = new ArrayList<String>();
		Scanner s = new Scanner(new File(filePath));
		while (s.hasNext()){
		    list.add(s.next());
		}
		s.close();
		return list;
	}

}