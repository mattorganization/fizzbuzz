package com.mpo.fizzbuzz.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.io.FileNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.mpo.fizzbuzz.dao.StatisticDAO;
import com.mpo.fizzbuzz.model.GenerationParameters;
import com.mpo.fizzbuzz.model.GenerationStatistic;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {StatisticService.class, StatisticDAO.class})
public class StatisticServiceTest {
	
	private final GenerationParameters generation1 = new GenerationParameters(3, 5, 100, "fizz", "buzz");
	private final GenerationParameters generation2 = new GenerationParameters(4, 7, 1000, "LeBon", "Coin");
	
	@Autowired
    private StatisticService statisticService;
	
	@BeforeEach
	public void initTest() {
		statisticService.clearStatistics();
	}

	@Test
	public void noRequestHitTest() throws FileNotFoundException {
		GenerationStatistic generationStatisticRetrieved = statisticService.getMostHitRequest();
		
		assertNull(generationStatisticRetrieved);
	}

	@ParameterizedTest
	@ValueSource(ints = {1, 2, 1_000_000})
	public void oneRequestHitsTest(int nbhits) throws FileNotFoundException {
		for (int i = 0; i < nbhits; i++) {
			statisticService.addHit(generation1.getFizzReplaced(), generation1.getBuzzReplaced(), generation1.getLimit(), generation1.getFizzReplacing(), generation1.getBuzzReplacing());
		}
		GenerationStatistic generationStatisticRetrieved = statisticService.getMostHitRequest();
		
		assertEquals(generation1, generationStatisticRetrieved.getGenerationParameters());
		assertEquals(nbhits, generationStatisticRetrieved.getHits());
	}

	@ParameterizedTest
	@ValueSource(ints = {3, 5, 1_000_000})
	public void manyRequestHitsTest(int nbhits) throws FileNotFoundException {
		for (int i = 0; i < 2; i++) {
			statisticService.addHit(generation2.getFizzReplaced(), generation2.getBuzzReplaced(), generation2.getLimit(), generation2.getFizzReplacing(), generation2.getBuzzReplacing());
		}
		for (int i = 0; i < nbhits; i++) {
			statisticService.addHit(generation1.getFizzReplaced(), generation1.getBuzzReplaced(), generation1.getLimit(), generation1.getFizzReplacing(), generation1.getBuzzReplacing());
		}
		GenerationStatistic generationStatisticRetrieved = statisticService.getMostHitRequest();
		
		assertEquals(generation1, generationStatisticRetrieved.getGenerationParameters());
		assertEquals(nbhits, generationStatisticRetrieved.getHits());
	}

}