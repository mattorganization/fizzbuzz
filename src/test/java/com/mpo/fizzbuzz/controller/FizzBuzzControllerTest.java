package com.mpo.fizzbuzz.controller;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import com.mpo.fizzbuzz.service.FizzBuzzService;
import com.mpo.fizzbuzz.service.StatisticService;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

@ContextConfiguration(classes = { FizzBuzzController.class })
@WebMvcTest
public class FizzBuzzControllerTest {
	
	private final static int FIZZ_REPLACED_CLASSIC_FIZZ_BUZZ = 3;
	private final static int BUZZ_REPLACED_CLASSIC_FIZZ_BUZZ = 5;
	private final static int LIMIT_VALUE_CLASSIC_FIZZ_BUZZ = 100;
	private final static String FIZZ_REPLACING_CLASSIC_FIZZ_BUZZ = "fizz";
	private final static String BUZZ_REPLACING_CLASSIC_FIZZ_BUZZ = "buzz";
	
	private final static int FIZZ_REPLACED_LEBONCOIN_FIZZ_BUZZ = 4;
	private final static int BUZZ_REPLACED_LEBONCOIN_FIZZ_BUZZ = 7;
	private final static int LIMIT_VALUE_LEBONCOIN_FIZZ_BUZZ = 1000;
	private final static String FIZZ_REPLACING_LEBONCOIN_FIZZ_BUZZ = "LeBon";
	private final static String BUZZ_REPLACING_LEBONCOIN_FIZZ_BUZZ = "Coin";
	
	@MockBean
	FizzBuzzService mockFizzBuzzService;
	
	@MockBean
	StatisticService mockStatisticService;

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void emptyResultFizzBuzzTest() throws Exception {
		Mockito.when(mockFizzBuzzService.generateFizzBuzz(FIZZ_REPLACED_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACED_CLASSIC_FIZZ_BUZZ, LIMIT_VALUE_CLASSIC_FIZZ_BUZZ, FIZZ_REPLACING_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACING_CLASSIC_FIZZ_BUZZ))
				.thenReturn(new ArrayList<String>());
		
		mockMvc.perform(get("/generate")
			.accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
			.andExpect(status().isOk())
			.andExpect(content().string("[]"));
		
		verify(mockFizzBuzzService, Mockito.times(1)).generateFizzBuzz(FIZZ_REPLACED_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACED_CLASSIC_FIZZ_BUZZ, LIMIT_VALUE_CLASSIC_FIZZ_BUZZ, FIZZ_REPLACING_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACING_CLASSIC_FIZZ_BUZZ);
		verify(mockStatisticService, Mockito.times(1)).addHit(FIZZ_REPLACED_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACED_CLASSIC_FIZZ_BUZZ, LIMIT_VALUE_CLASSIC_FIZZ_BUZZ, FIZZ_REPLACING_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACING_CLASSIC_FIZZ_BUZZ);
	}
	
	@Test
	public void simpleResultFizzBuzzTest() throws Exception {
		List<String> fizzBuzzList = new ArrayList<String>();
		fizzBuzzList. add("fizz");
		fizzBuzzList. add("buzz");
		Mockito.when(mockFizzBuzzService.generateFizzBuzz(FIZZ_REPLACED_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACED_CLASSIC_FIZZ_BUZZ, LIMIT_VALUE_CLASSIC_FIZZ_BUZZ, FIZZ_REPLACING_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACING_CLASSIC_FIZZ_BUZZ))
				.thenReturn(fizzBuzzList);
		
		mockMvc.perform(get("/generate")
			.accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
			.andExpect(status().isOk())
			.andExpect(content().string("[\"fizz\",\"buzz\"]"));
		
		verify(mockFizzBuzzService, Mockito.times(1)).generateFizzBuzz(FIZZ_REPLACED_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACED_CLASSIC_FIZZ_BUZZ, LIMIT_VALUE_CLASSIC_FIZZ_BUZZ, FIZZ_REPLACING_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACING_CLASSIC_FIZZ_BUZZ);
		verify(mockStatisticService, Mockito.times(1)).addHit(FIZZ_REPLACED_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACED_CLASSIC_FIZZ_BUZZ, LIMIT_VALUE_CLASSIC_FIZZ_BUZZ, FIZZ_REPLACING_CLASSIC_FIZZ_BUZZ, BUZZ_REPLACING_CLASSIC_FIZZ_BUZZ);
	}
	
	@Test
	public void simpleResultLeBonCoinFizzBuzzTest() throws Exception {
		List<String> fizzBuzzList = new ArrayList<String>();
		fizzBuzzList. add("LeBon");
		fizzBuzzList. add("Coin");
		Mockito.when(mockFizzBuzzService.generateFizzBuzz(FIZZ_REPLACED_LEBONCOIN_FIZZ_BUZZ, BUZZ_REPLACED_LEBONCOIN_FIZZ_BUZZ, LIMIT_VALUE_LEBONCOIN_FIZZ_BUZZ, FIZZ_REPLACING_LEBONCOIN_FIZZ_BUZZ, BUZZ_REPLACING_LEBONCOIN_FIZZ_BUZZ))
				.thenReturn(fizzBuzzList);
		
		mockMvc.perform(get("/generate?fizzReplaced=" + FIZZ_REPLACED_LEBONCOIN_FIZZ_BUZZ 
				+ "&buzzReplaced=" + BUZZ_REPLACED_LEBONCOIN_FIZZ_BUZZ
				+ "&limit=" + LIMIT_VALUE_LEBONCOIN_FIZZ_BUZZ
				+ "&fizzReplacing=" + FIZZ_REPLACING_LEBONCOIN_FIZZ_BUZZ
				+ "&buzzReplacing=" + BUZZ_REPLACING_LEBONCOIN_FIZZ_BUZZ)
			.accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
			.andExpect(status().isOk())
			.andExpect(content().string("[\"LeBon\",\"Coin\"]"));
		
		verify(mockFizzBuzzService, Mockito.times(1)).generateFizzBuzz(FIZZ_REPLACED_LEBONCOIN_FIZZ_BUZZ, BUZZ_REPLACED_LEBONCOIN_FIZZ_BUZZ, LIMIT_VALUE_LEBONCOIN_FIZZ_BUZZ, FIZZ_REPLACING_LEBONCOIN_FIZZ_BUZZ, BUZZ_REPLACING_LEBONCOIN_FIZZ_BUZZ);
		verify(mockStatisticService, Mockito.times(1)).addHit(FIZZ_REPLACED_LEBONCOIN_FIZZ_BUZZ, BUZZ_REPLACED_LEBONCOIN_FIZZ_BUZZ, LIMIT_VALUE_LEBONCOIN_FIZZ_BUZZ, FIZZ_REPLACING_LEBONCOIN_FIZZ_BUZZ, BUZZ_REPLACING_LEBONCOIN_FIZZ_BUZZ);
	}

}