package com.mpo.fizzbuzz.app;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles(profiles = {"context-load-test"})
class ApplicationMainTests {

	@Test
	void contextLoads() {
	}

}