package com.mpo.fizzbuzz.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.mpo.fizzbuzz.model.GenerationParameters;
import com.mpo.fizzbuzz.model.GenerationStatistic;

@Repository
public class StatisticDAO {
	
	private final Map<GenerationParameters, Integer> generationParametersHits;
	
	public StatisticDAO() {
		generationParametersHits = new HashMap<>();
	}
	
	/**
	 * Get stored statistic for given parameters.
	 * 
	 * @param fizzReplaced Numeric value to replace as a fizz.
	 * @param buzzReplaced Numeric value to replace as a buzz.
	 * @param limit List size.
	 * @param fizzReplacing The fizz replacing string.
	 * @param buzzReplacing The buzz replacing string.
	 * @return @see GenerationStatistic The parameters combination with the number of hits for this, null if no result found.
	 */
	public GenerationStatistic getParametersStatistic(int fizzReplaced, int buzzReplaced, int limit, String fizzReplacing, String buzzReplacing) {
		GenerationParameters generationParameters = new GenerationParameters(fizzReplaced, buzzReplaced, limit, fizzReplacing, buzzReplacing);
		Integer hits = generationParametersHits.get(generationParameters);
		if (hits == null)
			return null;
		else
			return new GenerationStatistic(generationParameters, hits);
	}
	
	/**
	 * 
	 * Store parameters combination with a given number of hits.
	 * 
	 * @param fizzReplaced Numeric value to replace as a fizz.
	 * @param buzzReplaced Numeric value to replace as a buzz.
	 * @param limit List size.
	 * @param fizzReplacing The fizz replacing string.
	 * @param buzzReplacing The buzz replacing string.
	 * @param hits Number of hits for the parameters combination.
	 */
	public GenerationStatistic createParametersStatistic(int fizzReplaced, int buzzReplaced, int limit, String fizzReplacing, String buzzReplacing, int hits) {
		GenerationParameters generationParameters = new GenerationParameters(fizzReplaced, buzzReplaced, limit, fizzReplacing, buzzReplacing);
		generationParametersHits.put(generationParameters, hits);
		return new GenerationStatistic(generationParameters, hits);
	}
	
	/**
	 * 
	 * Save the given statistic.
	 * 
	 * @param generationStatistic @see GenerationStatistic Statistic to save.
	 */
	public void saveStatistic(GenerationStatistic generationStatistic) {
		generationParametersHits.put(generationStatistic.getGenerationParameters(), generationStatistic.getHits());
	}
	
	/**
	 * 
	 * Get the most hit parameters combination with number of hits.
	 * 
	 * @return @see GenerationStatistic The parameters combination with the number of hits for this, null if no result found.
	 */
	public GenerationStatistic getMostHitParametersStatistic() {
		Integer mostHitsRetrieved = null;
		GenerationParameters generationParametersRetrieved = null;
		
		for(Map.Entry<GenerationParameters, Integer> generationParametersHitsEntry : generationParametersHits.entrySet()) {
			if (mostHitsRetrieved == null ||
					mostHitsRetrieved < generationParametersHitsEntry.getValue()) {
				mostHitsRetrieved = generationParametersHitsEntry.getValue();
				generationParametersRetrieved = generationParametersHitsEntry.getKey();
			}
		}
		
		if (mostHitsRetrieved != null && generationParametersRetrieved != null)
			return new GenerationStatistic(generationParametersRetrieved, mostHitsRetrieved);
		else
			return null;
	}
	
	/**
	 * 
	 * Clear all stored statistics.
	 * 
	 */
	public void clearStatistics() {
		generationParametersHits.clear();
	}

}