package com.mpo.fizzbuzz.model;

/**
 * 
 * Fizz buzz parameters generation.
 *
 */
public class GenerationParameters {

	/**
	 * Numeric value to replace as a fizz.
	 */
	private int fizzReplaced;
	/**
	 * Numeric value to replace as a buzz.
	 */
	private int buzzReplaced;
	/**
	 * List size.
	 */
	private int limit;
	/**
	 * The fizz replacing string.
	 */
	private String fizzReplacing;
	/**
	 * The buzz replacing string.
	 */
	private String buzzReplacing;
	
	public GenerationParameters(int fizzReplaced, int buzzReplaced, int limit, String fizzReplacing, String buzzReplacing) {
		this.fizzReplaced = fizzReplaced;
		this.buzzReplaced = buzzReplaced;
		this.limit = limit;
		this.fizzReplacing = fizzReplacing;
		this.buzzReplacing = buzzReplacing;
	}

	public int getFizzReplaced() {
		return fizzReplaced;
	}

	public int getBuzzReplaced() {
		return buzzReplaced;
	}

	public int getLimit() {
		return limit;
	}

	public String getFizzReplacing() {
		return fizzReplacing;
	}

	public String getBuzzReplacing() {
		return buzzReplacing;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + buzzReplaced;
		result = prime * result + ((buzzReplacing == null) ? 0 : buzzReplacing.hashCode());
		result = prime * result + fizzReplaced;
		result = prime * result + ((fizzReplacing == null) ? 0 : fizzReplacing.hashCode());
		result = prime * result + limit;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GenerationParameters other = (GenerationParameters) obj;
		if (buzzReplaced != other.buzzReplaced)
			return false;
		if (buzzReplacing == null) {
			if (other.buzzReplacing != null)
				return false;
		} else if (!buzzReplacing.equals(other.buzzReplacing))
			return false;
		if (fizzReplaced != other.fizzReplaced)
			return false;
		if (fizzReplacing == null) {
			if (other.fizzReplacing != null)
				return false;
		} else if (!fizzReplacing.equals(other.fizzReplacing))
			return false;
		if (limit != other.limit)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "GenerationParameters [fizzReplaced=" + fizzReplaced + ", buzzReplaced=" + buzzReplaced + ", limit="
				+ limit + ", fizzReplacing=" + fizzReplacing + ", buzzReplacing=" + buzzReplacing + "]";
	}

}