package com.mpo.fizzbuzz.model;

/**
 * 
 * Class to store fizz buzz parameters statistics.
 *
 */
public class GenerationStatistic {

	/**
	 * @see GenerationParameters Fizz buzz parameters used
	 */
	private GenerationParameters generationParameters;
	
	/**
	 * Number of hits for the parameters combination @see GenerationStatistic#generationParameters
	 */
	private int hits;
	
	public GenerationStatistic(int fizzReplaced, int buzzReplaced, int limit, String fizzReplacing, String buzzReplacing) {
		this.generationParameters = new GenerationParameters(fizzReplaced, buzzReplaced, limit, fizzReplacing, buzzReplacing);
		hits = 0;
	}
	
	public GenerationStatistic(GenerationParameters generationParameters, int hits) {
		this.generationParameters = generationParameters;
		this.hits = hits;
	}
	
	public GenerationParameters getGenerationParameters() {
		return generationParameters;
	}
	
	public int getHits() {
		return hits;
	}
	
	public void addHit() {
		hits++;
	}
	
	@Override
	public String toString() {
		return "GenerationStatistic [generationParameters=" + generationParameters + ", hits=" + hits + "]";
	}

}