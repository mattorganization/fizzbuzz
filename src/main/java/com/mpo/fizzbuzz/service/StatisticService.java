package com.mpo.fizzbuzz.service;

import org.springframework.stereotype.Service;

import com.mpo.fizzbuzz.dao.StatisticDAO;
import com.mpo.fizzbuzz.model.GenerationStatistic;

@Service
public class StatisticService {
	
	private final StatisticDAO statisticDAO;
	
	public StatisticService(StatisticDAO statisticDAO) {
		this.statisticDAO = statisticDAO;
	}

	/**
	 * 
	 * Add a hit for the given parameters.
	 * 
	 * @param fizzReplaced Numeric value to replace as a fizz.
	 * @param buzzReplaced Numeric value to replace as a buzz.
	 * @param limit List size.
	 * @param fizzReplacing The fizz replacing string.
	 * @param buzzReplacing The buzz replacing string.
	 */
	public void addHit(int fizzReplaced, int buzzReplaced, int limit, String fizzReplacing, String buzzReplacing) {
		GenerationStatistic generationStatistic = statisticDAO.getParametersStatistic(fizzReplaced, buzzReplaced, limit, fizzReplacing, buzzReplacing);
		if (generationStatistic == null)
			generationStatistic = statisticDAO.createParametersStatistic(fizzReplaced, buzzReplaced, limit, fizzReplacing, buzzReplacing, 0);
		generationStatistic.addHit();
		statisticDAO.saveStatistic(generationStatistic);
	}
	
	/**
	 * 
	 * Get the most hit parameters combination with number of hits.
	 * 
	 * @return @see GenerationStatistic The parameters combination with the number of hits for this, null if no result found.
	 */
	public GenerationStatistic getMostHitRequest() {
		return statisticDAO.getMostHitParametersStatistic();
	}
	
	/**
	 * 
	 * Clear all statistics saved.
	 * 
	 */
	public void clearStatistics() {
		statisticDAO.clearStatistics();
	}

}