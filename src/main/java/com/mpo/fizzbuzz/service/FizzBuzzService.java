package com.mpo.fizzbuzz.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class FizzBuzzService {

	public static final int START_FIZZ_BUZZ = 1;
	public static final int MIN_FIZZ_BUZZ_LIMIT = 0;
	public static final int MAX_FIZZ_BUZZ_LIMIT = 1_000_000;
	public static final int MIN_FIZZ_BUZZ_REPLACED = START_FIZZ_BUZZ;
	public static final int MAX_FIZZ_BUZZ_REPLACED = MAX_FIZZ_BUZZ_LIMIT;
	
	/**
	 * 
	 * Generate a fizz buzz list from 1 to 'limit' value.
	 * 
	 * @param fizzReplaced Numeric value to replace as a fizz, from 1 to 1.000.000.
	 * @param buzzReplaced Numeric value to replace as a buzz, from 1 to 1.000.000.
	 * @param limit List size, from 0 to 1.000.000.
	 * @param fizzReplacing The fizz replacing string, not null or empty.
	 * @param buzzReplacing The buzz replacing string, not null or empty.
	 * @return The fizz buzz list.
	 * @throws IllegalArgumentException When fizzReplaced, buzzReplaced, limit, fizzReplacing or buzzReplacing don't match constraints above.
	 */
	public List<String> generateFizzBuzz(int fizzReplaced, int buzzReplaced, int limit, String fizzReplacing, String buzzReplacing)
			throws IllegalArgumentException {
		if (fizzReplaced < MIN_FIZZ_BUZZ_REPLACED || fizzReplaced > MAX_FIZZ_BUZZ_REPLACED)
			throw new IllegalArgumentException("Fizz replaced value is out of range");
		if (buzzReplaced < MIN_FIZZ_BUZZ_REPLACED || buzzReplaced > MAX_FIZZ_BUZZ_REPLACED)
			throw new IllegalArgumentException("Buzz replaced value is out of range");
		if (limit < MIN_FIZZ_BUZZ_LIMIT || limit > MAX_FIZZ_BUZZ_LIMIT)
			throw new IllegalArgumentException("Limit value is out of range");
		if (fizzReplacing == null || fizzReplacing.isEmpty())
			throw new IllegalArgumentException("Fizz replacing value must not be null or empty");
		if (buzzReplacing == null || buzzReplacing.isEmpty())
			throw new IllegalArgumentException("Buzz replacing value must not be null or empty");
		
		List<String> fizzBuzzList = new ArrayList<>();
		
		for (int i = START_FIZZ_BUZZ; i <=  limit; i++) {
			if (i % fizzReplaced == 0 && i % buzzReplaced == 0)
				fizzBuzzList.add(fizzReplacing + buzzReplacing);
			else if (i % fizzReplaced == 0)
				fizzBuzzList.add(fizzReplacing);
			else if (i % buzzReplaced == 0)
				fizzBuzzList.add(buzzReplacing);
			else
				fizzBuzzList.add("" + i);
		}
		
		return fizzBuzzList;
	}

}