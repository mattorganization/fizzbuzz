package com.mpo.fizzbuzz.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mpo.fizzbuzz.model.GenerationStatistic;
import com.mpo.fizzbuzz.service.FizzBuzzService;
import com.mpo.fizzbuzz.service.StatisticService;

@RestController
public class FizzBuzzController {
	
	private final FizzBuzzService fizzBuzzService;
	
	private final StatisticService statisticService;
	
	public FizzBuzzController(FizzBuzzService fizzBuzzService, StatisticService statisticService) {
		this.fizzBuzzService = fizzBuzzService;
		this.statisticService = statisticService;
	}
	
	/**
	 * 
	 * REST endpoint to generate a fizz buzz list.
	 * 
	 * @param fizzReplaced Numeric value to replace as a fizz, from 1 to 1.000.000, default to 3.
	 * @param buzzReplaced Numeric value to replace as a buzz, from 1 to 1.000.000, default to 5.
	 * @param limit List size, from 0 to 1.000.000, default to 100.
	 * @param fizzReplacing The fizz replacing string, default to "fizz".
	 * @param buzzReplacing The buzz replacing string, default to "buzz".
	 * @return The fizz buzz list, with HTTP 200 if everything is OK, HTTP 400 when fizzReplaced, buzzReplaced, limit, fizzReplacing or buzzReplacing don't match constraints above.
	 */
	@GetMapping("/generate")
	public List<String> getFizzBuzz(@RequestParam(defaultValue = "3") int fizzReplaced,
			@RequestParam(defaultValue = "5") int buzzReplaced,
			@RequestParam(defaultValue = "100") int limit,
			@RequestParam(defaultValue = "fizz") String fizzReplacing,
			@RequestParam(defaultValue = "buzz") String buzzReplacing) 
					throws IllegalArgumentException {
		List<String> fizzBuzzList = fizzBuzzService.generateFizzBuzz(fizzReplaced, buzzReplaced, limit, fizzReplacing, buzzReplacing);
		statisticService.addHit(fizzReplaced, buzzReplaced, limit, fizzReplacing, buzzReplacing);
		return fizzBuzzList;
	}
	
	/**
	 * 
	 * REST endpoint to get most parameters hit to generate fizz buzz list.
	 * 
	 * @return @see GenerationStatistic The most hit parameters and numbers of hits, with HTTP 200.
	 */
	@GetMapping("/mosthitgeneration")
	public GenerationStatistic getMostHitGeneration() {
		return statisticService.getMostHitRequest();
	}

}