package com.mpo.fizzbuzz.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * 
 * Class to catch exceptions and manage HTTP response.
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	/**
	 * 
	 * Manage IllegalArgumentException to return HTTP 400 and error message in body.
	 * 
	 * @param ex @see IllegalArgumentException.
	 * @return Error message with HTTP 400.
	 */
	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<String> generateException(IllegalArgumentException ex) {
		return new ResponseEntity<String>(ex.getMessage(), HttpStatus.BAD_REQUEST);
	}
  
}