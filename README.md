# Fizz buzz service

## What is it ?

It's a REST API web server made with maven and spring boot.

You can generate a fizz buzz list giving the list size, the values to replace and the values to replace with.

You can also get the most hit parameters used to generate the fizz buzz list. Warning: these satistics are reset at each application startup.

## How to use it

### Using java

Application main jar can be generated with the `mvn install` command.

Application can be launched with `java -jar <generated jar name>`, web server would be accessible on port 8080 with root path /fizzbuzz.

### Using docker

After main jar generation, you can build docker image using command `docker build -t <tag for your docker image> .` on root directory.

Then run the image using command `docker run -p <exposing port of your choice>:8080 <tag of your docker image>`.

## APIs

### Generate fizz buzz list
----
  Public API to generate a fizz buzz list depending on parameters.

* **URL**

  /generate

* **Method:**
  
  `GET`
  
*  **URL Params**

   **Optional:**
 
   `fizzReplaced=[numeric]` Fizz number to replace (from 1 to 1.000.000), default 3
   
   `buzzReplaced=[numeric]` Buzz number to replace (from 1 to 1.000.000), default 5
   
   `limit=[numeric]` Numbers list is from 1 to limit (from 0 to 1.000.000), default 100
   
   `fizzReplacing=[alphanumeric]` String replacing fizz number, default "fizz"
   
   `buzzReplacing=[alphanumeric]` String replacing buzz number, default "buzz"

* **Success Response:**

  * **Code:** 200
  
    **Content example:** `["1","2","fizz","4","buzz","fizz","7","8","fizz","buzz","11","fizz","13","14","fizzbuzz"]`
 
* **Error Response:**

  * **Code:** 400 BAD REQUEST
  
    **Content example:** `Fizz replaced value is out of range`

* **Sample Call:**

  curl -X GET "http://localhost:8080/fizzbuzz/generate?fizzReplacing=LeBon&buzzReplacing=Coin&limit=200&fizzReplaced=20&buzzReplaced=50"

### Fizz buzz list parameters most hit
----
  Public API to get most parameters hit to generate fizz buzz list.

* **URL**

  /mosthitgeneration

* **Method:**
  
  `GET`
  
*  **URL Params**

   **No parameter**

* **Success Response:**

  * **Code:** 200
  
    **Content example:** No value when not hit or`{"generationParameters":{"fizzReplaced":3,"buzzReplaced":5,"limit":10,"fizzReplacing":"fizz","buzzReplacing":"buzz"},"hits":4}`
 

* **Sample Call:**

  curl -X GET "http://localhost:8080/fizzbuzz/mosthitgeneration"